let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .js('resources/assets/js/admin/app-admin.js', 'public/admin/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('resources/assets/sass/admin/app-admin.scss', 'public/admin/css')
   .copy('node_modules/font-awesome/css/font-awesome.min.css', 'public/admin/css/font-awesome.min.css')
   .copy('node_modules/simple-line-icons/css/simple-line-icons.css', 'public/admin/css/simple-line-icons.css')
   .copyDirectory('node_modules/font-awesome/fonts', 'public/admin/fonts')
   .copyDirectory('node_modules/simple-line-icons/fonts', 'public/admin/fonts')
   .copyDirectory('resources/assets/img', 'public/img');

mix.version();

mix.browserSync('company-studio.dev');
