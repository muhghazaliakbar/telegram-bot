<?php

Route::get('/', function () {
    return view('welcome');
})->name('index');

Route::get('home', 'HomeController@index')->name('home');

// Login Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
