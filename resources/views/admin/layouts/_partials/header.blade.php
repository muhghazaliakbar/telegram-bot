<header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none" type="button">☰</button>
    <a class="navbar-brand" href=""></a>
    <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item">
            <a class="nav-link navbar-toggler sidebar-toggler" href="#">☰</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="{{ route('index') }}" target="_blank">Home</a>
        </li>
    </ul>
    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" href="#">
                <img src="{{ asset('img/avatar.jpg') }}" class="img-avatar" alt="{{ auth()->user()->name }}">
                <span class="d-md-down-none">{{ auth()->user()->name }}</span>
            </a>
        </li>
    </ul>
    <button class="navbar-toggler"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
        <i class="icon-logout"></i>
    </button>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
</header>
