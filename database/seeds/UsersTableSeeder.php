<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Storage\User::class)->create(['name' => 'Muh Ghazali Akbar', 'email' => 'muhghazaliakbar@live.com']);
    }
}
